const { exec } = require('child_process');

// Start the backend in the 'backend' directory in the background
exec('cd backend && npm start > /dev/null 2>&1 &', (error, stdout, stderr) => {
  if (error) {
    console.error(`Error starting backend: ${error}`);
    return;
  }
});

// Start the root application in the root directory (foreground)
exec('npm start', (error, stdout, stderr) => {
  if (error) {
    console.error(`Error starting root application: ${error}`);
    return;
  }
});
