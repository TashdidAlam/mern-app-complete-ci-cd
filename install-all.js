const { exec } = require('child_process');

// Install root-level dependencies
exec('npm install', (error, stdout, stderr) => {
  if (error) {
    console.error(`Error installing root-level dependencies: ${error}`);
    return;
  }
  console.log(stdout);

  // Change directory to the 'backend' folder and install its dependencies
  exec('cd backend && npm install', (error, stdout, stderr) => {
    if (error) {
      console.error(`Error installing backend dependencies: ${error}`);
      return;
    }
    console.log(stdout);

    console.log('All dependencies installed successfully.');
  });
});
