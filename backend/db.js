const mongoose = require('mongoose');
const mongoURI = "mongodb+srv://tashdid7068:1157068@cluster0.tg1v6ts.mongodb.net/gofoodmern?retryWrites=true&w=majority";

const mongoDB = async () => {
    try {
        await mongoose.set('strictQuery', false);
        await mongoose.connect(mongoURI, { useNewUrlParser: true });
        console.log('Mongo connected');
        
        // Define a schema and a model for your 'food_items' collection
        const foodItemSchema = new mongoose.Schema({
            // Define your schema fields here
            // For example, name: String, description: String, etc.
        });
        const FoodItem = mongoose.model('food_item', foodItemSchema);

        // Use the model to find and retrieve data
        const data = await FoodItem.find({});
        console.log();

    } catch (error) {
        console.log(error);
        process.exit(1);
    }
}

module.exports = mongoDB;
